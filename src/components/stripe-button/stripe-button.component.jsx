import React from "react";
import StripeCheckout from "react-stripe-checkout";

const StripeCheckoutButton = ({price}) => {
    const priceForStripe = price * 100;
    const publushableKey = 'pk_test_51KQc7qGwhIpJDODomzGTpTmcXuyBVPvgh742Ugw05oeOowKAagsEqUZZRO7LJosHog9a6VFHEO4zkR9kbB1ph4Hn00uRHxEgp4';

    const onToken = token => {
        console.log(token);
        alert('Payment Successfful');
    }

    return (
    <StripeCheckout 
        label='Pay Now'
        name='CRWN CLOTHING'
        billingAddress
        shippingAddress
        image='https://svgshare.com/i/CUz.svg'
        description={`Total is $${price}`}
        amount={priceForStripe}
        panelLabel="Pay Now"
        token={onToken}
        stripeKey={publushableKey}
    />
    )
}


export default StripeCheckoutButton;

