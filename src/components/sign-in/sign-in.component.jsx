import React from "react";


import FormInput from "../form-input/form-input.component";
import CustomButton from "../custom-button/custom-button.component";

import { signInWithGoogle, auth } from "../../firebase/firebase.utils";
import { signInWithEmailAndPassword } from 'firebase/auth';

import './sign-in.styles.scss';


class SignIn extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
        }
    }


    handleSubmit = async event => {
        event.preventDefault()

        const {email, password} = this.state;

        try {
            signInWithEmailAndPassword(auth, email, password)
            
            this.setState({ password: '', email: ''})
        } catch(error) {
            console.log(error);
        }

        this.setState({ password: '', email: ''})
    }

    handleChange = event => {
        const {value, name} = event.target

        this.setState({[name]: value})
    }


    render() {
        return (
        <div className="sign-in">
            <h1> I already have an account </h1>
            <span> Sign in with your email and password </span>

            <form onSubmit={this.handleSubmit}>
                <FormInput label='email' name='email' value={this.state.email} type='email' handleChange={this.handleChange} required/>

                <FormInput label='password' name='password' value={this.state.password} type='password' handleChange={this.handleChange} required/>

                <div className="buttons">
                    <CustomButton type='submit'> Sign in </CustomButton>
                    <CustomButton type="button" onClick={signInWithGoogle} isGoogleSignIn>
                        Sign in with Google 
                    </CustomButton>
                </div>
                


            </form>
        </div>
        );
    }



}


export default SignIn;

