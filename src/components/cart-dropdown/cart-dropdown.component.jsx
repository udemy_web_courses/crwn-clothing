import React from "react";
import CustomButton from "../custom-button/custom-button.component";
import { createStructuredSelector } from "reselect";

import './cart-dropdown.styles.scss';
import CartItem from "../cart-item/cart-item.component";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { selectCartItems } from "../../redux/cart/cart.selectors";
import { toggleCartHidden } from '../../redux/cart/cart.actions'


const CartDropdown = ({cartItems, dispatch}) => {
    const navigate = useNavigate();
    console.log('navigate    ', navigate)
    return (
    <div className="cart-dropdown">
        <div className="cart-items">
            { cartItems.length ?
             cartItems.map(item => <CartItem key={item.id} item={item} />)
             :
            <span className="empty-message"> Your cart is empty </span>
            }
        </div>
        <CustomButton onClick={() => {
            dispatch(toggleCartHidden())
            navigate('/checkout')
        }}> 
            GO TO CHECKOUT 
        </CustomButton>
    </div>
);
}

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems,
})



export default connect(mapStateToProps)(CartDropdown);
