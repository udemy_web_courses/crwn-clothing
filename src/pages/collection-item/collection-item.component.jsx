import React from "react";
import { connect } from "react-redux";


import "./collection-item.styles.scss";
import CustomButton from "../../components/custom-button/custom-button.component";
import { addItem } from '../../redux/cart/cart.actions'


const CollectionItem = ({item, item:{id, name, imageUrl, price}, addItem}) => (

    <div className="collection-item">
        <div 
        className="image"
        style={{
            backgroundImage: `url(${imageUrl})`,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'end',
        }}
        saf={console.log(`url(${imageUrl})`)}
        >
            <CustomButton style={{margin: '28px 12px'}} inverted onClick={() => addItem(item)}> Add to cart </CustomButton>
        </div>
        <div className="collection-footer">
            <span className="name"> {name} </span>
            <span className="price"> {price} </span>
        </div>
    </div>
)


const mapDispatchToProps = dispatch => ({
    addItem: item => dispatch(addItem(item))
})

export default connect(null, mapDispatchToProps)(CollectionItem);