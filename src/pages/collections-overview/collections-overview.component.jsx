import React from "react";
import CollectionPreview from '../collection-preview/collection-preview.component'

import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect'
import { selectCollectionForPreview } from "../../redux/shop/shop.selectors";


import './collections-overview.styles.scss';

const CollectionsOverview = ({collections}) => {
    console.log('collections   ', collections)

    return (
    <div className="collections-overview">
        {
            collections.map(({id, ...other}) => <CollectionPreview key={id} {...other}/>)
        }
    </div>
)
    }

const mapStateToProps = createStructuredSelector({
    collections: selectCollectionForPreview,
})


export default connect(mapStateToProps)(CollectionsOverview);

