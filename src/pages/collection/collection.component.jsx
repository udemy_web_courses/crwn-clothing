import React from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectCollection } from "../../redux/shop/shop.selectors";
import CollectionItem from '../collection-item/collection-item.component'

import './collection.styles.scss';

const myWithRouterHOC = WrappedComponent => (props = {'title' : 'props of HOC'}) => {
    const params = useParams();
    // etc... other react-router-dom v6 hooks
  
    return (
      <WrappedComponent
        {...props}
        params={params}
        // etc...
      />
    );
  };


/*const HocToUseParams = (ReactComp) => props => {
    const params = useParams();

    return <CollectionPage params={params} />
}*/

const CollectionPage = ({collection}) => {
    
  const {title, items} = collection;
    
    return (
                <div className="collection-page">
                  <h2 className="title"> {title} </h2>
                  <div className="items">
                      {
                        items.map(item => <CollectionItem key={item.id} item={item} />)
                      }
                  </div>
                </div>
        );
}

const mapStateToProps = (state, ownProps) => {
    console.log('ownProps  ', ownProps)

    return ({
    collection: selectCollection(ownProps.params.collectionId)(state)
})

}


export default myWithRouterHOC(connect(mapStateToProps)(myWithRouterHOC(CollectionPage)));

