import React from "react";
import { Route, Routes } from "react-router-dom";
import { connect } from "react-redux";
import CollectionPage from "../collection/collection.component";

import { updateCollections } from "../../redux/shop/shop.actions";
import CollectionsOverview from "../collections-overview/collections-overview.component";
import { firestore, convertCollectionsSnapshotToMap } from '../../firebase/firebase.utils';
import { collection, onSnapshot } from 'firebase/firestore';

class ShopPage extends React.Component {

    unsubscribeFromSnapchot = null;

    componentDidMount() {
        const collectionRef = collection(firestore, 'collections');
        
        this.unsubscribeFromSnapchot = onSnapshot(collectionRef, async snapshot => {
            const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
            console.log('mark 1')
            updateCollections(collectionsMap);
            this.setState({shop:{collections: collectionsMap}})
            console.log('mark 2')

        })

    }



render(){
    console.log('props at shop.component   ', this.props)
    return (
            <div className="shop-page">
                <Routes>
                    <Route path='/' element={<CollectionsOverview />} />
                    <Route path=':collectionId' element={<CollectionPage />} />
                </Routes>
               
            </div>
    )
} 
}   

const dispatchToProps = dispatch => ({
    updateCollections: collectionsMap => dispatch(updateCollections(collectionsMap)),
})

export default connect(null, dispatchToProps)(ShopPage);

