import React from 'react';
import {
  Route,
  Routes,
  Navigate,
  
  } from 'react-router-dom';
import { connect } from 'react-redux';
import { selectCurrentUser } from './redux/user/user.selectors';
import { createStructuredSelector } from 'reselect';

import './App.css';


import Homepage from './pages/homepage/homepage.component.jsx';
import ShopPage from './pages/shop/shop.component.jsx';
import SignInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';
import CheckoutPage from './pages/checkout/checkout.component';


import Header from './components/header/header.component';

import { auth, createUserProfileDocument, convertCollectionsSnapshotToMap} from './firebase/firebase.utils';


import { setCurrentUser } from './redux/user/user.actions';

import { onSnapshot } from 'firebase/firestore';


class App extends React.Component {



  unsubscribeFromAuth = null;

  componentDidMount() {

    const { setCurrentUser } = this.props;

    this.unsubscribeFromAuth = auth.onAuthStateChanged( async userAuth => {
      if(userAuth) {
        //this.setState({ currentUser: user })
        const userRef = await createUserProfileDocument(userAuth)

        onSnapshot(userRef, snapShot => {
          setCurrentUser({
            id: snapShot.id,
            ...snapShot.data(),
            })
          })
        

      }
        setCurrentUser(userAuth);
        //addCollectionAddDocuments('collections', collectionsArray.map(({title, items}) => ({title: title, items: items})));
      
    })
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth()
  }


  render() {
    return (
      <div>
        <Header />
        <Routes>
  
          <Route path="/" element={<Homepage />} />
          <Route path='/shop/*' element={<ShopPage />} />
          <Route path='/checkout' element={<CheckoutPage />} />

          <Route path='/signin' element={this.props.currentUser ? <Navigate to='/'/> : <SignInAndSignUpPage />} />
          <Route path="/*" element={<h1> Page not found </h1>} />
  
  
  
        </Routes>
      </div>
    );
  }
  
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
})

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(App);

