export const addItemToCart = (cartItems, cartItemAdd) => {
    const existingCartItem = cartItems.find(item => item.id === cartItemAdd.id)

    if(existingCartItem) {
        return cartItems.map(item =>
            item.id === cartItemAdd.id
            ? {...item, quantity: item.quantity + 1}
            : item
            )
    
    } else {
        return [...cartItems, {...cartItemAdd, quantity: 1}]
    }

}


export const removeItemFromCart = (cartItems, cartItemToRemove) => {
    const existingCartItem = cartItems.find(item => item.id === cartItemToRemove.id)

    if(existingCartItem.quantity === 1) {
        return cartItems.filter( item => item.id !== cartItemToRemove.id )
    } else {
        return cartItems.map(item =>
            item.id === cartItemToRemove.id
            ? {...item, quantity: item.quantity - 1}
            : item
            )
    }

}