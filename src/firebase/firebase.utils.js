/*import * as firebase from "firebase/app"
import 'firebase/firestore';
import 'firebase/auth';*/


import { initializeApp } from "firebase/app"
import { getFirestore, collection, doc, getDocs, setDoc, getDoc, writeBatch } from "firebase/firestore"
import { getAuth, GoogleAuthProvider, connectAuthEmulator, signInWithEmailAndPassword, signInWithPopup } from "firebase/auth";





const config = {
    apiKey: "AIzaSyCuCmR5_95e2J5Ub549ALRSWq5HBROjRnA",
    authDomain: "crwn-db-1de51.firebaseapp.com",
    projectId: "crwn-db-1de51",
    storageBucket: "crwn-db-1de51.appspot.com",
    messagingSenderId: "887517662965",
    appId: "1:887517662965:web:845697d2dcf0ff616affbc",
    measurementId: "G-BME9RLQQ6L"
  };

export const firebaseApp = initializeApp(config);

export const firestore = getFirestore(firebaseApp);



export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;


  const userRef = doc(firestore, `users/${userAuth.uid}`)
  const snapshot = await getDoc(userRef)


  if(!snapshot.exists()) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {

      await setDoc(userRef, {
        displayName: displayName,
        email,
        createdAt,
        ...additionalData,
      })
    } catch(err) {
      console.log('error creating user   ', err.message)
    }

  }

  return userRef;

}

//export const firestore = firebase.firestore();


export const db = getFirestore(firebaseApp);

//export const auth = firebase.getAuth();

export const auth = getAuth(firebaseApp);


export const provider = new GoogleAuthProvider();
//const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => signInWithPopup(auth, provider);

//export default firebase;

export const addCollectionAddDocuments = async (collectionKey, objectsToAdd) => {
  const collectionRef = collection(firestore, collectionKey);

  const batch = writeBatch(firestore);
  objectsToAdd.forEach(obj => {
    const newDocRef = doc(collectionRef, obj.title);
    batch.set(newDocRef, obj);

  })

  return batch.commit();
}


export const convertCollectionsSnapshotToMap = async (collectionsSnapshot) => {
  //console.log('collections  ', collections)
  const transformedCollection = collectionsSnapshot.docs.map(docSnapshot => {
    console.log('doc    ', docSnapshot.data())
    const { title, items } = docSnapshot.data();
    return {
      routeName: encodeURI(title.toLowerCase()),
      id: docSnapshot.id,
      title,
      items,
    }
  })

  
  const result =  transformedCollection.reduce((acc, collection) => {
    acc[collection.title.toLowerCase()] = collection;
    return acc;
  }, {});

  console.log('result    ', result)

  return result;

}

